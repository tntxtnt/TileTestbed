﻿using UnityEngine;
using System.Collections;

public partial class Map
{
    private int width;
    public int Width { get { return width; } }
    private int height;
    public int Height { get { return height; } }
    public int thickness;
    private int cellWidth, cellHeight;
    private Cell[,] grid;
    public Cell[,] Grid { get { return grid; } }

    public Map(int width, int height, int cellWidth, int cellHeight, int thickness)
    {
        this.thickness = thickness;
        this.width = width;
        this.height = height;
        this.cellWidth = cellWidth + thickness;
        this.cellHeight = cellHeight + thickness;
        grid = new Cell[height, width];
        for (int r = 0; r < height; ++r)
            for (int c = 0; c < width; ++c)
                grid[r, c] = new Cell(r, c);
    }

    public void Draw(TilemapGenerator tilemap)
    {
        // vertical
        int top = height * cellHeight + thickness/2;
        int left = -thickness/2;
        int w = thickness;
        int h = height * cellHeight;
        tilemap.CreateTile(top, left, w, h);
        bool detachTop = false, detachBot = false;
        for (int c = 0; c < width; ++c)
        {
            left += cellWidth;
            h = 0;
            for (int r = 0; r < height; ++r)
            {
                Map.Cell cell = grid[r, c];
                if (cell.HasRightWall)
                {
                    if (h == 0)
                    {
                        top = (height - r) * cellHeight + thickness / 2;  /////
                        detachTop = !cell.HasTopWall && (c + 1 >= width || !grid[r, c + 1].HasTopWall);
                        if (detachTop)
                            top -= thickness / 2;
                    }
                    h += cellHeight;
                }
                else if (h > 0)
                {
                    detachBot = !cell.HasTopWall && (c + 1 >= width || !grid[r, c + 1].HasTopWall);
                    if (detachTop && detachBot) h -= thickness;
                    else if (detachTop || detachBot) h -= thickness / 2;
                    tilemap.CreateTile(top, left, w, h + thickness); ////
                    h = 0;
                }
            }
            if (h > 0)
            {
                if (detachTop) h -= thickness / 2;
                tilemap.CreateTile(top, left, w, h + thickness);
            }
        }

        // horizontal 
        top = height * cellHeight + thickness/2;
        left = -thickness/2;
        w = width * cellWidth;
        h = thickness;
        tilemap.CreateTile(top, left, w, h);
        for (int r = 0; r < height; ++r)
        {
            top -= cellHeight;
            left = w = 0;
            for (int c = 0; c < width; ++c)
            {
                Map.Cell cell = grid[r, c];
                if (cell.HasBottomWall)
                {
                    if (w == 0)
                    {
                        left = cellWidth * c - thickness / 2; ////
                        detachTop = !cell.HasLeftWall && (r + 1 >= height || !grid[r + 1, c].HasLeftWall); //detachLeft
                        if (detachTop)
                            left += thickness / 2;
                    }
                    w += cellWidth;
                }
                else if (w > 0) 
                {
                    detachBot = !cell.HasLeftWall && (r + 1 >= height || !grid[r + 1, c].HasLeftWall); //detachRight
                    if (detachTop && detachBot) w -= thickness;
                    else if (detachTop || detachBot) w -= thickness / 2;
                    tilemap.CreateTile(top, left, w + thickness, h);
                    w = 0;
                }
            }
            if (w > 0)
            {
                if (detachTop) w -= thickness / 2;
                tilemap.CreateTile(top, left, w + thickness, h);
            }
        }
    }

    public void OpenWall(Cell cur, Cell.WallDirection wallDir, Cell opp)
    {
        grid[cur.RowId, cur.ColId].OpenWall(wallDir);
        grid[opp.RowId, opp.ColId].OpenOppositeWall(wallDir);
    }
}





public partial class Map //Map.Cell
{
    public class Cell
    {
        public enum WallDirection { Up = 0, Left = 1, Down = 2, Right = 3 }

        private int r, c;
        public int RowId { get { return r; } }
        public int ColId { get { return c; } }
        public bool[] walls = new bool[4] { true, true, true, true };

        public Cell(int r, int c)
        {
            this.r = r;
            this.c = c;
        }

        public bool IsInaccessible
        {
            get
            {
                foreach (bool wall in walls) if (!wall) return false;
                return true;
            }
        }

        public void OpenWall(WallDirection dir)
        {
            walls[(int)dir] = false;
        }

        public void OpenOppositeWall(WallDirection dir)
        {
            int d = ((int)dir + 2) % 4;
            walls[d] = false;
        }

        public bool HasTopWall { get { return walls[(int)Cell.WallDirection.Up]; } }
        public bool HasLeftWall { get { return walls[(int)Cell.WallDirection.Left]; } }
        public bool HasBottomWall { get { return walls[(int)Cell.WallDirection.Down]; } }
        public bool HasRightWall { get { return walls[(int)Cell.WallDirection.Right]; } }
    }
}