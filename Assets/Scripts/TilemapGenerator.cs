﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Neighbor = System.Collections.Generic.KeyValuePair<Map.Cell.WallDirection, Map.Cell>;

public class TilemapGenerator : MonoBehaviour
{
    public GameObject terrain;

    public int mapWidth = 15;
    public int mapHeight = 10;
    public int cellWidth = 10;
    public int cellHeight = 5;
    public int cellThickness = 4;
    public bool random = true;
    public bool useStringSeed = false;
    public string seedStr;
    public int seedInt;

    private Map map;
    private GameObject mapHolder;
    private GameObject tileHolder;
    private GameObject platfHolder;

    void GenerateDfsMaze()
    {
        List<Map.Cell> inaccessibleCells = new List<Map.Cell>();
        for (int r = 0; r < map.Height; ++r)
            for (int c = 0; c < map.Width; ++c)
                if (map.Grid[r, c].IsInaccessible)
                    inaccessibleCells.Add(map.Grid[r, c]);

        Map.Cell cur = inaccessibleCells[Random.Range(0, inaccessibleCells.Count)];
        Stack<Map.Cell> q = new Stack<Map.Cell>();
        while (true)
        {
            List<Neighbor> unvisitedNeighbors = new List<KeyValuePair<Map.Cell.WallDirection, Map.Cell>>();
            if (cur.RowId > 0 && map.Grid[cur.RowId - 1, cur.ColId].IsInaccessible)
                unvisitedNeighbors.Add(new Neighbor(Map.Cell.WallDirection.Up, map.Grid[cur.RowId - 1, cur.ColId]));
            if (cur.RowId + 1 < map.Height && map.Grid[cur.RowId + 1, cur.ColId].IsInaccessible)
                unvisitedNeighbors.Add(new Neighbor(Map.Cell.WallDirection.Down, map.Grid[cur.RowId + 1, cur.ColId]));
            if (cur.ColId > 0 && map.Grid[cur.RowId, cur.ColId - 1].IsInaccessible)
                unvisitedNeighbors.Add(new Neighbor(Map.Cell.WallDirection.Left, map.Grid[cur.RowId, cur.ColId - 1]));
            if (cur.ColId + 1 < map.Width && map.Grid[cur.RowId, cur.ColId + 1].IsInaccessible)
                unvisitedNeighbors.Add(new Neighbor(Map.Cell.WallDirection.Right, map.Grid[cur.RowId, cur.ColId + 1]));

            if (unvisitedNeighbors.Count > 0)
            {
                Neighbor next = unvisitedNeighbors[Random.Range(0, unvisitedNeighbors.Count)];
                q.Push(cur);
                map.OpenWall(cur, next.Key, next.Value);
                cur = next.Value;
            }
            else if (q.Count > 0)
            {
                cur = q.Pop();
            }
            else break;
        }
    }

    public void CreateTile(int top, int left, int width, int height)
    {
        var tile = Instantiate(terrain) as GameObject;
        tile.transform.position = Tile.Scale(left, top);
        tile.transform.localScale = new Vector3(width, height, 1);
        tile.transform.SetParent(tileHolder.transform);
    }

    public void RandomMap()
    {
        map = new Map(mapWidth, mapHeight, cellWidth, cellHeight, cellThickness);
        mapHolder = new GameObject();
        tileHolder = new GameObject();
        tileHolder.transform.SetParent(mapHolder.transform);
        tileHolder.name = "Tiles";
        platfHolder = new GameObject();
        platfHolder.transform.SetParent(mapHolder.transform);
        platfHolder.name = "Platforms";

        if (!random)
        {
            if (useStringSeed && seedStr.Length > 0)
                Random.seed = seedStr.GetHashCode();
            else
                Random.seed = seedInt;
        }
        //Random.seed = -1940921060;
        mapHolder.name = "Map " + Random.seed + " " + mapWidth + " " + mapHeight
            + " " + cellWidth + " " + cellHeight + " " + cellThickness;
        GenerateDfsMaze();
        map.Draw(this);

        mapHolder = tileHolder = platfHolder = null;
    }
}


//Create Button to allow the Sync while in Editor
#if UNITY_EDITOR
[CustomEditor(typeof(TilemapGenerator))]
public class MapGenEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TilemapGenerator myScript = (TilemapGenerator)target;
        if (GUILayout.Button("Random map (delete old map first"))
            myScript.RandomMap();
    }
}
#endif