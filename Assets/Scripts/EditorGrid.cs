﻿using UnityEngine;
using System.Collections;

public class EditorGrid : MonoBehaviour
{
    public bool isOn = true;
    public int rowCount = 100;
    public int columnCount = 100;

    void OnDrawGizmos()
    {
        if (!isOn) return;

        // draw horizontal lines
        Vector3 from = Tile.Scale(-rowCount / 2, -columnCount / 2);
        Vector3 to = Tile.Scale(rowCount / 2, -columnCount / 2);
        for (int i = 0; i <= columnCount; ++i, from.y += Tile.height, to.y += Tile.height)
            Gizmos.DrawLine(from, to);

        // draw vertical lines
        from = Tile.Scale(-rowCount / 2, -columnCount / 2);
        to = Tile.Scale(-rowCount / 2, columnCount / 2);
        for (int i = 0; i <= rowCount; ++i, from.x += Tile.width, to.x += Tile.width)
            Gizmos.DrawLine(from, to);
    }
}
