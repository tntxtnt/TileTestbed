﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    public static float width = 0.48f;
    public static float height = 0.48f;

    public static Vector2 Scale(int x, int y)
    {
        return new Vector2(x * width, y * height);
    }

    public static string NameFromRowCol(int r, int c)
    {
        return "[" + r + "," + c + "]";
    }

    public static Vector2 HalfTileOffset(bool top, bool left)
    {
        return new Vector2((left ? -.5f : .5f) * width, (top ? .5f : -.5f) * height);
    }


    private void SyncScale()
    {
        float scaleX = (int)Mathf.Round(transform.lossyScale.x);
        float scaleY = (int)Mathf.Round(transform.lossyScale.y);
        transform.localScale = new Vector3(scaleX, scaleY, 1);
    }

    private void SyncScaleX()
    {
        float scaleX = (int)Mathf.Round(transform.lossyScale.x);
        transform.localScale = new Vector3(scaleX, transform.localScale.y, 1);
    }

    private void SyncPosition()
    {
        float posX = (int)Mathf.Round(transform.position.x / width) * width;
        float posY = (int)Mathf.Round(transform.position.y / height) * height;
        transform.position = new Vector2(posX, posY);
    }

    private void SyncPositionPlatform()
    {
        float halfWidth = width / 2f;
        float halfHeight = height / 2f;
        float posX = (int)Mathf.Round(transform.position.x / halfWidth) * halfWidth;
        float posY = (int)Mathf.Round(transform.position.y / halfHeight) * halfHeight;
        transform.position = new Vector2(posX, posY);
    }

    public void SyncTile()
    {
        SyncScale();
        SyncPosition();
    }

    public void SyncPlatform()
    {
        SyncScaleX();
        SyncPositionPlatform();
    }
}



//Create Button to allow the Sync while in Editor
#if UNITY_EDITOR
[CustomEditor(typeof(Tile))]
public class UpdateTiles : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Tile myScript = (Tile)target;
        if (GUILayout.Button("Sync Tile"))
            myScript.SyncTile();
        if (GUILayout.Button("Sync Platform"))
            myScript.SyncPlatform();
    }
}
#endif