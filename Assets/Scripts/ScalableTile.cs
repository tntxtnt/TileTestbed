﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Require pivot at top left corner
/// </summary>
public class ScalableTile : MonoBehaviour
{
    public Transform targetTexture;
    public GameObject tilePrefab;
    public Sprite[] tileSprites;
    public Sprite[] innerTileSprites;
    public LayerMask whatIsTile;

    private int rows, cols;

    public class RecursiveMerge
    {
        public bool recursive;
        public HashSet<GameObject> visited;
        public RecursiveMerge(bool recursive = false, HashSet<GameObject> visited = null)
        {
            this.recursive = recursive;
            this.visited = visited;
        }
    }

    void Awake()
    {
        var sRend = GetComponent<SpriteRenderer>();
        if (sRend) sRend.enabled = false; //disable current sprite renderer if there is one
        CreateTileTexture();
    }

    void Start()
    {
        MergeCollisions(new RecursiveMerge());
    }

    public int GetTileId(int r, int c)
    {
        // 0 1 1 1 2
        // 3 4 4 4 5
        // 3 4 4 4 5
        // 6 7 7 7 8
        int tileId = 4;
        if (r == 0) tileId = 1; //top
        else if (r == rows - 1) tileId = 7; //bottom
        if (c == 0) tileId--; //left
        else if (c == cols - 1) tileId++; //right
        return tileId;
    }

    public void CreateTileTexture()
    {
        // clear texture (aka kill all children of targetTexture)
        for (int i = targetTexture.childCount; i-- > 0; )
            GameObject.Destroy(targetTexture.transform.GetChild(i).gameObject);

        // get dimensions
        rows = (int)Mathf.Round(transform.lossyScale.y);
        cols = (int)Mathf.Round(transform.lossyScale.x);

        // instantiate (rows x cols) tiles
        for (int r = 0; r < rows; ++r)
        {
            for (int c = 0; c < cols; ++c)
            {
                GameObject child = Instantiate(tilePrefab) as GameObject;
                child.GetComponent<SpriteRenderer>().sprite = tileSprites[GetTileId(r, c)];
                child.transform.position = transform.position + (Vector3)Tile.Scale(c, -r);
                child.transform.SetParent(targetTexture);
                child.name = Tile.NameFromRowCol(r, c);
            }
        }
    }

    public void ResetSprites()
    {
        var texture = targetTexture;
        for (int r = 0; r < rows; ++r)
            for (int c = 0; c < cols; ++c)
                texture.FindChild(Tile.NameFromRowCol(r, c)).GetComponent<SpriteRenderer>().sprite = tileSprites[GetTileId(r, c)];
    }

    public void MergeCollisions(RecursiveMerge rm)
    {
        // check collision
        HashSet<GameObject> colliders = new HashSet<GameObject>();
        Vector2 p = transform.position + (Vector3)Tile.HalfTileOffset(true, true);
        foreach (var hit in Physics2D.RaycastAll(p, Vector2.down, (rows + 1) * Tile.height, whatIsTile))
            colliders.Add(hit.transform.parent.transform.parent.gameObject);
        foreach (var hit in Physics2D.RaycastAll(p, Vector2.right, (cols + 1) * Tile.width, whatIsTile))
            colliders.Add(hit.transform.parent.transform.parent.gameObject);
        p += Tile.Scale(cols + 1, -rows - 1);
        foreach (var hit in Physics2D.RaycastAll(p, Vector2.up, (rows + 1) * Tile.height, whatIsTile))
            colliders.Add(hit.transform.parent.transform.parent.gameObject);
        foreach (var hit in Physics2D.RaycastAll(p, Vector2.left, (cols + 1) * Tile.width, whatIsTile))
            colliders.Add(hit.transform.parent.transform.parent.gameObject);
        // merge
        foreach (var obj in colliders)
            MergeTile(obj);
        // call others
        if (!rm.recursive) return;
        if (rm.visited == null)
            rm.visited = new HashSet<GameObject>();
        rm.visited.Add(gameObject);
        foreach (var obj in colliders)
            if (!rm.visited.Contains(obj))
                obj.BroadcastMessage("ResetSprites");
        foreach (var obj in colliders)
            if (!rm.visited.Contains(obj))
                obj.BroadcastMessage("MergeCollisions", rm);
    }

    private void MergeTile(GameObject that)
    {
        int left1 = (int)Mathf.Round(this.transform.position.x / Tile.width);
        int top1 = (int)Mathf.Round(this.transform.position.y / Tile.height);
        int right1 = left1 + (int)Mathf.Round(this.transform.lossyScale.x);
        int bottom1 = top1 - (int)Mathf.Round(this.transform.lossyScale.y);
        int left2 = (int)Mathf.Round(that.transform.position.x / Tile.width);
        int top2 = (int)Mathf.Round(that.transform.position.y / Tile.height);
        int right2 = left2 + (int)Mathf.Round(that.transform.lossyScale.x);
        int bottom2 = top2 - (int)Mathf.Round(that.transform.lossyScale.y);

        int top = Math.Min(top1, top2);
        int left = Math.Max(left1, left2);
        int width = Math.Min(right1, right2) - left;
        int height = top - Math.Max(bottom1, bottom2);

        // merge this
        if (width < 0 || height < 0)
            return;
        else if (width == 0 || height == 0)
            MergeTouch(top1 - top, left - left1, width, height);
        else
            MergeOverlap(top1 - top, left - left1, that, top2 - top, left - left2, width, height);
    }

    private bool IsInnerCorner(Transform s, bool top, bool left)
    {
        if (s == null) return false;
        Vector2 p = Tile.HalfTileOffset(top, left) + (Vector2)s.transform.position;
        if (!top) p += Vector2.down * Tile.height;
        if (!left) p += Vector2.right * Tile.width;
        HashSet<GameObject> nearH = new HashSet<GameObject>();
        HashSet<GameObject> nearV = new HashSet<GameObject>();
        foreach (var hit in Physics2D.RaycastAll(p, top ? Vector2.down : Vector2.up, Tile.height, whatIsTile))
            nearH.Add(hit.transform.parent.transform.parent.gameObject);
        foreach (var hit in Physics2D.RaycastAll(p, left ? Vector2.right : Vector2.left, Tile.width, whatIsTile))
            nearV.Add(hit.transform.parent.transform.parent.gameObject);
        if (nearH.Count == 0 || nearV.Count == 0)
            return false;
        HashSet<GameObject> farV = new HashSet<GameObject>();
        foreach (var hit in Physics2D.RaycastAll(p, left ? Vector2.left : Vector2.right, Tile.width, whatIsTile))
            farV.Add(hit.transform.parent.transform.parent.gameObject);
        farV.IntersectWith(nearV);
        return farV.Count == 0;
    }

    private void MergeTouch(int i, int j, int width, int height)
    {
        Transform texture = targetTexture;
        Transform s = null;
        if (width == 0) // || stand next to each other
        {
            if (j > 0) --j;
            for (int h = 0; h < height; ++h, ++i)
            {
                if ((s = texture.FindChild(Tile.NameFromRowCol(i, j))) == null) continue;
                var sSpriteRenderer = s.GetComponent<SpriteRenderer>();
                int spriteId = int.Parse(sSpriteRenderer.sprite.name.Split('_')[1]);
                int newSpriteId = GetTileId(i, j) + (j == 0 ? 1 : -1);
                if (spriteId != GetTileId(i, j) && spriteId != newSpriteId) newSpriteId = 4;
                sSpriteRenderer.sprite = tileSprites[newSpriteId];
            }
            // INNER CORNERS
            if (IsInnerCorner(s = texture.FindChild(Tile.NameFromRowCol(i - height, j)), true, j == 0))
                s.GetComponent<SpriteRenderer>().sprite = innerTileSprites[j == 0 ? 0 : 1]; //top + left or right
            if (IsInnerCorner(s = texture.FindChild(Tile.NameFromRowCol(i - 1, j)), false, j == 0))
                s.GetComponent<SpriteRenderer>().sprite = innerTileSprites[j == 0 ? 2 : 3]; //bottom + left or right
            // END - INNER CORNERS
        }
        else if (height == 0) // = one on top of another 
        {
            if (i > 0) --i;
            for (int w = 0; w < width; ++w, ++j)
            {
                if ((s = texture.FindChild(Tile.NameFromRowCol(i, j))) == null) continue;
                var sSpriteRenderer = s.GetComponent<SpriteRenderer>();
                int spriteId = int.Parse(sSpriteRenderer.sprite.name.Split('_')[1]);
                int newSpriteId = GetTileId(i, j) + (i == 0 ? 3 : -3);
                if (spriteId != GetTileId(i, j) && spriteId != newSpriteId) newSpriteId = 4;
                sSpriteRenderer.sprite = tileSprites[newSpriteId];
            }
            // INNER CORNERS
            if (IsInnerCorner(s = texture.FindChild(Tile.NameFromRowCol(i, j - width)), i == 0, true))
                s.GetComponent<SpriteRenderer>().sprite = innerTileSprites[i == 0 ? 0 : 2]; //top or bottom + left
            if (IsInnerCorner(s = texture.FindChild(Tile.NameFromRowCol(i, j - 1)), i == 0, false))
                s.GetComponent<SpriteRenderer>().sprite = innerTileSprites[i == 0 ? 1 : 3]; //top or bottom + right
            // END - INNER CORNERS
        }
    }

    private void MergeOverlap(int top1, int left1, GameObject that, int top2, int left2, int width, int height)
    { /// only have to check the border
        Transform texture = targetTexture;
        ScalableTile st2 = that.GetComponent<ScalableTile>();
        Transform s = null;
        int i, j;
        for (int w = 0; w < width; ++w)
        {
            // top row
            if ((s = texture.FindChild(Tile.NameFromRowCol(top1, left1 + w))) == null) continue;
            i =     GetTileId(top1, left1 + w);
            j = st2.GetTileId(top2, left2 + w);
            s.GetComponent<SpriteRenderer>().sprite = tileSprites[overlap[i, j]];
            // bottom row
            if ((s = texture.FindChild(Tile.NameFromRowCol(top1 + height - 1, left1 + w))) == null) continue;
            i =     GetTileId(top1 + height - 1, left1 + w);
            j = st2.GetTileId(top2 + height - 1, left2 + w);
            s.GetComponent<SpriteRenderer>().sprite = tileSprites[overlap[i, j]];
        }
        for (int h = 1; h < height - 1; ++h)
        {
            // left column
            if ((s = texture.FindChild(Tile.NameFromRowCol(top1 + h, left1))) == null) continue;
            i =     GetTileId(top1 + h, left1);
            j = st2.GetTileId(top2 + h, left2);
            s.GetComponent<SpriteRenderer>().sprite = tileSprites[overlap[i, j]];
            // right column
            if ((s = texture.FindChild(Tile.NameFromRowCol(top1 + h, left1 + width - 1))) == null) continue;
            i =     GetTileId(top1 + h, left1 + width - 1);
            j = st2.GetTileId(top2 + h, left2 + width - 1);
            s.GetComponent<SpriteRenderer>().sprite = tileSprites[overlap[i, j]];
        }
        // INNER CORNERS
        if (IsInnerCorner(s = texture.FindChild(Tile.NameFromRowCol(top1, left1)), true, true))
            s.GetComponent<SpriteRenderer>().sprite = innerTileSprites[0]; //top + left
        if (IsInnerCorner(s = texture.FindChild(Tile.NameFromRowCol(top1, left1 + width - 1)), true, false))
            s.GetComponent<SpriteRenderer>().sprite = innerTileSprites[1]; //top + right
        if (IsInnerCorner(s = texture.FindChild(Tile.NameFromRowCol(top1 + height - 1, left1)), false, true))
            s.GetComponent<SpriteRenderer>().sprite = innerTileSprites[2]; //bottom + left
        if (IsInnerCorner(s = texture.FindChild(Tile.NameFromRowCol(top1 + height - 1, left1 + width - 1)), false, false))
            s.GetComponent<SpriteRenderer>().sprite = innerTileSprites[3]; //bottom + right
        // END - INNER CORNERS
    }


    private static int[,] overlap = {
        { 0, 1, 1,
          3, 4, 4,
          3, 4, 4 },
        { 1, 1, 1,
          4, 4, 4,
          4, 4, 4 },
        { 1, 1, 2,
          4, 4, 5,
          4, 4, 5 },
        { 3, 4, 4,
          3, 4, 4,
          3, 4, 4 },
        { 4, 4, 4,
          4, 4, 4,
          4, 4, 4 },
        { 4, 4, 5,
          4, 4, 5,
          4, 4, 5 },
        { 3, 4, 4,
          3, 4, 4,
          6, 7, 7 },
        { 4, 4, 4,
          4, 4, 4,
          7, 7, 7 },
        { 4, 4, 5,
          4, 4, 5,
          7, 7, 8 }
    };
}




//Create Button to allow the Update while in Editor
#if UNITY_EDITOR
[CustomEditor(typeof(ScalableTile))]
public class UpdateTextures : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ScalableTile myScript = (ScalableTile)target;
        if (GUILayout.Button("Update Texture (Runtime only)"))
            myScript.CreateTileTexture();
        else if (GUILayout.Button("Reset Sprites (Runtime only)"))
            myScript.ResetSprites();
        else if (GUILayout.Button("Merge Collisions (Runtime only)"))
            myScript.MergeCollisions(new ScalableTile.RecursiveMerge(true));
    }
}
#endif
