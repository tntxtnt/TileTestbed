﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Require pivot at top left corner
/// </summary>
public class ScalablePlatform : MonoBehaviour
{
    public Transform targetTexture;
    public GameObject platfPrefab;
    public Sprite[] platfSprites;
    public LayerMask whatIsPlatf;

    private int length;

    void Awake()
    {
        var sRend = GetComponent<SpriteRenderer>();
        if (sRend) sRend.enabled = false; //disable current sprite renderer if there is one
        CreatePlatformTexture();
    }

    public int GetPlatformId(int x)
    {
        // 0 1 1 1 2 or 3
        if (length == 1) return 3;
        if (x == 0) return 0;
        if (x == length - 1) return 2;
        return 1;
    }

    public void CreatePlatformTexture()
    {
        // clear texture
        for (int i = targetTexture.childCount; i-- > 0; )
            GameObject.Destroy(targetTexture.transform.GetChild(i).gameObject);

        // get length
        length = (int)Mathf.Round(transform.lossyScale.x);

        // instantiate (length) tiles
        for (int i = 0; i < length; ++i)
        {
            GameObject child = Instantiate(platfPrefab) as GameObject;
            child.GetComponent<SpriteRenderer>().sprite = platfSprites[GetPlatformId(i)];
            child.transform.position = transform.position + (Vector3)Tile.Scale(i, 0);
            child.transform.SetParent(targetTexture);
            child.name = Tile.NameFromRowCol(0, i);
        }

    }
}




//Create Button to allow the Update while in Editor
#if UNITY_EDITOR
[CustomEditor(typeof(ScalablePlatform))]
public class UpdatePlatformTextures : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ScalablePlatform myScript = (ScalablePlatform)target;
        if (GUILayout.Button("Update Texture (Runtime only)"))
            myScript.CreatePlatformTexture();
    }
}
#endif
